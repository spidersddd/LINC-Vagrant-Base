#!/bin/sh
## Bootstrap a Puppet Master

cd /vagrant

## Install some needed software
yum install -y git || (echo "git failed to install; exiting." && exit 1)
/opt/puppet/bin/gem install r10k --no-ri --no-rdoc || (echo "r10k failed to install; exiting" && exit 1)

/opt/puppet/bin/r10k puppetfile install -v || (echo "r10k didn't exit cleanly; exiting" && exit 1)

## Now bootstrap the master
## We just run a puppet apply with the role we have and specify our working
## directory to locate modules
/opt/puppet/bin/puppet apply -e 'include role::puppet::master' \
    --modulepath="/vagrant/modules:/vagrant/site:/opt/puppet/share/puppet/modules"

if [ $? -ne 0 ]; then
    echo "The Puppet run didn't exit cleanly.  This might be okay, but you should"
    echo "investigate."
fi
