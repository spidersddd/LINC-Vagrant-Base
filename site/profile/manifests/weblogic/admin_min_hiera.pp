class profile::weblogic::admin_min_hiera {

  $wls_os_user = hiera('wls_os_user')
  $wls_os_group = hiera('wls_os_group')
  $install = [ 'binutils.x86_64', 'unzip.x86_64' ]
  File {
    owner => $wls_os_user,
    group => $wls_os_group,
    mode  => '0775'
  }
    
  contain profile::weblogic::java


  anchor { 'begin': }
  -> 
  group { $wls_os_group:
    ensure => 'present',
  }
  -> 
  user { $wls_os_user:
    ensure     => 'present',
    gid        => $wls_os_group,
    shell      => '/bin/bash',
    managehome => true,
    require    => Group["$wls_os_group"],
  }
  ->
  package { $install:
    ensure => 'installed',
  }
  ->
  class { 'weblogic::provision': }
  ->
  weblogic::domain { 'PuppetTest':
#    version                              => '1213',
#    weblogic_home_dir                    => '/opt/was/oracle/middleware12c/wlserver',
#    middleware_home_dir                  => '/opt/was/oracle/middleware12c',
#    jdk_home_dir                         => '/usr/java/default',
#    domain_template                      => 'standard',
#    domain_name                          => 'PuppetTest',
#    development_mode                     => false,
#    adminserver_name                     => 'AdminServer',
#    adminserver_port                     => '7001',
#    adminserver_listen_on_all_interfaces => true,
#    nodemanager_secure_listener          => true,
#    nodemanager_port                     => '5556',
#    weblogic_user                        => 'oracle',
#    weblogic_password                    => 'password1',
#    os_user                              => 'oracle',
#    os_group                             => 'dba',
#    log_dir                              => '/var/log/wl',
#    download_dir                         => '/install_software/oracle',
#    log_output                           => true,
#    require                              => Class['profile::weblogic::java', 'jdk7'],
  }
#  ->
#  weblogic::control{'startWLSAdminServer12c':
#    domain_name                 => "PuppetTest",
#    server_type                 => 'admin',  # admin|managed
#    target                      => 'Server', # Server|Cluster
#    server                      => 'AdminServer',
#    action                      => 'start',
#    weblogic_home_dir           => "/opt/was/oracle/middleware12c/wlserver",
#    jdk_home_dir                => "/usr/java/default",
#    weblogic_user               => "oracle",
#    weblogic_password           => "password1",
#    adminserver_address         => 'localhost',
#    adminserver_port            => 7001,
#    nodemanager_port            => 5556,
#    nodemanager_secure_listener => true,
#    os_user                     => "oracle",
#    os_group                    => "dba",
#    download_dir                => "/data/install",
#    log_output                  => true,
#  }

  anchor { 'end': }
  
}
