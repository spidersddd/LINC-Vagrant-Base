class profile::weblogic::node{
  $wls_os_user = hiera('wls_os_user')
  $wls_os_group = hiera('wls_os_group')

  user { $wls_os_user:
    ensure     => 'present',
    gid        => $wls_os_group,
    shell      => '/bin/bash',
    managehome => true,
    require    => Group["$wls_os_group"],
  }

  include profile::weblogic::java
  include weblogic::utils::user
  include weblogic::os
  include weblogic::urandomfix
  include weblogic::ssh
  include weblogic::provision
  include orautils
  
  include weblogic::services::bsu
  include weblogic::services::fmw
  include weblogic::services::opatch
#  include weblogic::services::domains
  include weblogic::services::copydomain
  include weblogic::services::nodemanager
#  include weblogic::services::startwls
#  include weblogic::services::userconfig
  
##  include weblogic::services::security
#  include weblogic::services::basic_config
  
#  include weblogic::services::datasources
#  include weblogic::services::virtual_hosts
#  include weblogic::services::workmanagers
#  include weblogic::services::file_persistence
#  include weblogic::services::jms
#  include weblogic::services::pack_domain
  # include weblogic::services::cluster_control_instances
#  include weblogic::services::deployments

  Class['profile::weblogic::java'] ->
  Class['weblogic::utils::user'] ->
  Class['weblogic::os'] ->
  Class['weblogic::urandomfix'] ->
  Class['weblogic::ssh'] ->
  Class['weblogic::provision'] ->
  Class['orautils'] ->
  Class['weblogic::services::bsu'] ->
  Class['weblogic::services::fmw'] ->
  Class['weblogic::services::opatch'] ->
#  Class['weblogic::services::domains'] ->
  Class['weblogic::services::nodemanager']# ->
#  Class['weblogic::services::startwls']  ->
#  Class['weblogic::services::userconfig'] ->
#  Class['weblogic::services::security'] ->
#  Class['weblogic::services::basic_config'] ->
#  Class['weblogic::services::datasources'] # ->
#  Class['weblogic::services::virtual_hosts'] ->
#  Class['weblogic::services::workmanagers'] ->
#  Class['weblogic::services::file_persistence'] ->
#  Class['weblogic::services::jms'] ->
#  Class['weblogic::services::pack_domain'] ->
#  # Class['weblogic::services::cluster_control_instances']
#  Class['weblogic::services::deployments']
}
