# The master profile configures the base configuration for the master - the
# stuff that isn't managed by Puppet out of the box.  Custom stuff, like
# the Hiera configuration and r10k should go here.  You can classify masters
# with this to ensure they have a consistent configuration.
class profile::puppet::master {

  # Manage Hiera as a static file.
  file { 'hiera.yaml':
    ensure => 'file',
    path   => "${::settings::confdir}/hiera.yaml",
    source => 'puppet:///modules/profile/hiera.yaml',
  }

  # Core configuration of r10k.  Tell it where our control repository is and
  # where our environments are stored
  class { 'r10k':
    mcollective   => true,
    sources       => {
      'control'   => {
        'remote'  => '/vagrant',
        'basedir' => "${::settings::confdir}/environments",
        'prefix'  => false,
      },
    },
  }

  # This adds r10k to Puppet's "postrun" command.  That will ensure that r10k
  # runs at least everytime the agent on the master runs (default 30 min).
  # Adding it to the postrun instead of the "prerun" also ensures that we
  # don't prevent Puppet from running if r10k fails, since Puppet will not run
  # if its "prerun" command exits non-zero.
  include r10k::postrun_command

}
