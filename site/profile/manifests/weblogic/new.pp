class profile::weblogic::new {
  $domain_title = 'grins_domain'
  $user_name    = 'webadmin'
  $user_group   = 'webadmns'
  $wls_version  = '1213'
  $wls_oracle_base_home_dir = "/opt/was/oracle/$wls_version"
  $middleware_home_dir = "$wls_oracle_base_home_dir/middleware"
  $weblogic_home_dir = "$wls_oracle_base_home_dir/middleware/wlserver"
  $export_dir = "$wls_oracle_base_home_dir/exports"
  $wls_domains_dir = "$wls_oracle_base_dir/user_projects/domains"
  $wls_apps_dir = ""$wls_oracle_base_dir/apps"
  $fmw_infra = undef
  $jdk_home_dir = 
  $os_user
  $os_group
  $download_dir
  $source
  $remote_file
  $javaParameters
  $log_output
  $temp_directory

  class { 'weblogic::utils::user': 
    user_name       => $user_name,
    user_group      => $user_group,
    user_shell      => '/bin/bash',
    user_home       => '/home/webadmin',
    user_comment    => 'Middleware User created by Puppet',
    user_managehome => true,
  }
  @@host { "$::fqdn":
    ensure   => present,
    ip       => "$::ipaddress_eth1",
    tag      => $domain_title,
  }

  Host <<| tag == "$domain_title" |>> 

  include profile::weblogic::java
  include weblogic::urandomfix
  
  class { 'weblogic::ssh':
    os_user  => $user_name,
    os_group => $user_group,
  }

  class { 'weblogic::provision':
    version              => $wls_version,
    filename             => 'fmw_12.1.3.0.0_wls.jar',
    oracle_base_home_dir => /opt/was/oracle/$wls_version",
    
  Class['profile::weblogic::java'] ->
  Class['weblogic::urandomfix'] ->
  Class['weblogic::ssh']
}
 
