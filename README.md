# WebLogic Puppet Implementation

Work for implementing WebLogic with Puppet will go here.

There's three nodes provided to demonstrate a "core" WebLogic deployment:

| Node  | Description                | IP            |
| ----- | -------------------------- | ------------- |
| admin | Admin server and PE master | 192.168.30.10 |
| app   | Application server         | 192.168.30.11 |
| ohs   | Oracle HTTP Server         | 192.168.30.12 |

The credentials for the Puppet Enterprise Console are __admin/password__

## Getting Started

### Oracle Downloads

You'll need an account on Oracle's site to download these.  It's free.

__Oracle WebLogic Server 12c__

Get the _"Generic WebLogic Server and Coherence Installer (881 MB)"_

At the time of this writing, it's version 12.1.3

[Oracle WebLogic Server 12c - Generic Server](http://www.oracle.com/technetwork/middleware/downloads/index-087510.html):
([http://www.oracle.com/technetwork/middleware/downloads/index-087510.html](http://www.oracle.com/technetwork/middleware/downloads/index-087510.html))

__Oracle Java__

JDK 7u76 for Linux x64 (`jdk-7u76-linux-x64.tar.gz`)

Get the `.tar.gz`, not the RPM.

[http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html](http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html)

[http://www.oracle.com/technetwork/java/javase/downloads/jce-7-download-432124.html](http://www.oracle.com/technetwork/java/javase/downloads/jce-7-download-432124.html)

### Vagrant Environment

Use the Vagrant environment provided.

This Vagrant environment uses the [puppetlabs/centos6.6-64-nocm](https://atlas.hashicorp.com/puppetlabs/boxes/centos-6.6-64-nocm)
box.

All Vagrant provisioning data is located in the `provision/` directory,
including a `pe.sh` script and Puppet Enterprise answer files for a master and
agent role.

The `pe.sh` script will download the Puppet Enterprise installer to the
`provision/` directory if it's not already available, add relevant entries to
`/etc/hosts`, and run the Puppet Enterprise installer with the proper answer
file.

The __admin__ server needs to be brought up and provisioned first.  The
__admin__ server also acts as an all-in-one Puppet Enterprise (3.7.2) master.

```shell
vagrant up admin
```


